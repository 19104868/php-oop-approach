<center>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<form validateMe="number4.php" method="post">
             Name: <input type="text" name="username" required><br><br>
             Address <input type="text" name="address" required><br><br>
             Age: <input type="number" name="age" required> <br><br>
             Mobile Number: <input type="text" name="mobilenumber" required> <br>
             
             <input type="submit">
 </form>
</body>
</html>
<?php
class Validate
{

    public function __construct()
    {
        $this->require1 = isset($_POST['username']) ? $_POST['username'] : null;
        $this->require2 = isset($_POST['address']) ? $_POST['address'] : null;
        $this->require3 = isset($_POST['mobilenumber']) ? $_POST['mobilenumber'] : null;
        $this->require4 = isset($_POST['age']) ? $_POST['age'] : null;
    }

    public function validateHere()
    {
        if (!preg_match("/^[a-zA-z]*$/", $this->require1)) {
            $ErrMsg = "Only alphabets and whitespace are allowed.<br>";
            echo $ErrMsg;
        } else if($this->require1=="") {
            echo $this->require1 . " Please Enter a valid name<br>";
        }else{
            echo $this->require1 . " your Name is valid <br>";
        }

        if(!preg_match('/^(?:\\d+ [a-zA-Z ]+, ){2}[a-zA-Z ]+$/', $this->require2)){
            echo $this->require2 ." not valid address<br>";
        }else{
            echo $this->require2 ." Address is valid<br>";
        }
      

        if ($this->require4 >= 18) {
            echo $this->require4 ." is a legal age<br>";
        } else {
            echo $this->require4 ." is not a legal age<br>";
        }

        $length = strlen($this->require3);

        if ($length < 11 || $length > 11    ) {
            $ErrMsg = $this->require3 ." this must be 11 digits";
            echo $ErrMsg;
        } else {
            echo $this->require3 ." Phone number is valid";
        }
        
    }
}


$validate = new Validate();

$validate->validateHere();
?>
</center>